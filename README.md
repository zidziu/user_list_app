## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)

## General info
This project is a simple User list app that displays user data and has features to add a new user, delete the user and edit user info
	
## Technologies
Project is created with:
* React 
	
## Setup
To run this project, install it locally using npm:

```
$ npm install
$ npm start
```