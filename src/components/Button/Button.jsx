import React from "react";
import classNames from "classnames";
import PropTypes from "prop-types";

import "./button.scss";

function Button({
  type = "button",
  handleClick,
  children,
  borderOnly,
  pagination,
  disabled,
  withIcon,
  modifierClass = "",
  buttonRef,
  iconX,
  form,
  handleSubmit,
}) {
  const btnClass = classNames({
    button: true,
    "button--border-only": borderOnly,
    "button--pagination": pagination,
    "button--with-icon": withIcon,
    "button--icon-x": iconX,
    "button--form": form,
  });
  return (
    <button
      type={type}
      disabled={disabled}
      onClick={handleClick}
      ref={buttonRef}
      className={
        modifierClass === "" ? btnClass : `${btnClass} ${modifierClass}`
      }
      onSubmit={handleSubmit}
    >
      {children}
    </button>
  );
}

Button.propTypes = {
  type: PropTypes.string,
  handleClick: PropTypes.func,
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element,
    PropTypes.array,
  ]),
  blank: PropTypes.bool,
  pagination: PropTypes.bool,
  disabled: PropTypes.bool,
  blankWithBorder: PropTypes.bool,
  withIcon: PropTypes.bool,
  modifierClass: PropTypes.string,
  buttonRef: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.shape({ current: PropTypes.instanceOf(Element) }),
  ]),
  form: PropTypes.bool,
};

export default Button;
