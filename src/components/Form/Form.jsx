import React, { useState } from "react";
import PropTypes from "prop-types";
import Button from "../Button/Button";

import "./form.scss";

const Form = ({
  formTitle,
  action,
  buttonLabel,
  addNewUser = true,
  name,
  surname,
  email,
  image,
  id,
}) => {
  const [formData, setFormData] = useState({
    id: addNewUser ? "" : id,
    first_name: addNewUser ? "" : name,
    last_name: addNewUser ? "" : surname,
    email: addNewUser ? "" : email,
    avatar: addNewUser ? "" : image,
    first_nameValid: addNewUser ? false : true,
    last_nameValid: addNewUser ? false : true,
    emailValid: addNewUser ? false : true,
  });

  const [error, setError] = useState({
    first_name: "",
    last_name: "",
    email: "",
  });

  // validation function
  const checkValidity = (inputName, inputValue) => {
    switch (inputName) {
      case "first_name":
        formData.first_nameValid = inputValue.length >= 3;
        setFormData(formData.first_nameValid, true);

        break;
      case "last_name":
        formData.last_nameValid = inputValue.length >= 2;
        setFormData(formData.first_nameValid, true);
        break;
      case "email":
        let pattern = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/; // eslint-disable-line react-hooks/exhaustive-deps
        formData.emailValid = pattern.test(inputValue);

        break;
      default:
        break;
    }
  };

  // state update
  const handleChange = (e) => {
    checkValidity(e.target.name, e.target.value);
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  // form submit
  const onSubmitLoginForm = (e) => {
    let hasErrors = false;
    let errors = {};
    if (!formData.first_nameValid) {
      errors.first_name = "Name must be at least 3 characters long";
      hasErrors = true;
    }
    if (!formData.last_nameValid) {
      errors.last_name = "Last name must be at least 2 characters long";
      hasErrors = true;
    }
    if (!formData.emailValid) {
      errors.email = "Invalid email address";

      hasErrors = true;
    }
    setError(errors);
    if (hasErrors === false) {
      addNewUser
        ? action(
            e,
            formData.first_name,
            formData.last_name,
            formData.email,
            formData.avatar
          )
        : action(
            e,
            formData.id,
            formData.first_name,
            formData.last_name,
            formData.email,
            formData.avatar
          );
    }
  };

  return (
    <section className="form">
      <div className="form__header">
        <h1>{formTitle}</h1>
      </div>
      <form
        onSubmit={(e) => {
          e.preventDefault();
        }}
      >
        <fieldset className="form__content">
          <label htmlFor="first_name">Name</label>
          <input
            name="first_name"
            type="text"
            onChange={(e) => handleChange(e)}
            value={formData.first_name}
          />

          <label htmlFor="last_name">Last name </label>
          <input
            name="last_name"
            label="Last name"
            type="text"
            onChange={(e) => handleChange(e)}
            value={formData.last_name}
          />

          <label htmlFor="email">Email</label>
          <input
            label="Email"
            name="email"
            type="email"
            placeholder={"email@example.com"}
            onChange={(e) => handleChange(e)}
            value={formData.email}
          />

          <label htmlFor="avatar">Image link</label>
          <input
            label="Image link"
            name="avatar"
            type="text"
            placeholder={"https://example.com/image.jpg"}
            required={false}
            onChange={(e) => handleChange(e)}
            value={formData.avatar}
          />
        </fieldset>
        <Button type="submit" form handleClick={onSubmitLoginForm}>
          {buttonLabel}
        </Button>
        <p className="form__error-msg">{error.first_name}</p>
        <p className="form__error-msg">{error.last_name}</p>
        <p className="form__error-msg">{error.email}</p>
      </form>
    </section>
  );
};

Form.propTypes = {
  formTitle: PropTypes.string,
  buttonLabel: PropTypes.string,
  action: PropTypes.func,
  addNewUser: PropTypes.bool,
};

export default Form;
