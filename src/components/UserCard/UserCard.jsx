import React from "react";
import PropTypes from "prop-types";
import UserCardButtons from "./UserCardButtons";

import "./userCard.scss";

const UserCard = ({
  name,
  surname,
  email,
  image,
  userId,
  deleteAction,
  editAction,
}) => {
  return (
    <div className="UserCard">
      <div className="UserCard__name">
        <div className="UserCard__image">
        <img src={image} alt="User"></img>{" "}
        </div>
        <p>
          {name} {surname}
        </p>
      </div>


      <div className="UserCard__email">
        {" "}
        <p>{email} </p>
      </div>
      <div className="UserCard__actions">
        <UserCardButtons
          name={name}
          surname={surname}
          email={email}
          image={image}
          userId={userId}
          deleteAction={deleteAction}
          editAction={editAction}
        />
      </div>
    </div>
  );
};

UserCard.propTypes = {
  name: PropTypes.string,
  surname: PropTypes.string,
  email: PropTypes.string,
  image: PropTypes.string,
};

export default UserCard;
