import React, { useState, useRef } from "react";
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import Form from "../Form/Form";

import { ReactComponent as EditIcon } from "../../assets/icons/pen.svg";
import { ReactComponent as BinIcon } from "../../assets/icons/bin.svg";
import { useModal } from "../../utils/useModal";

import "./userCard.scss";

const UserCardButtons = ({
  userId,
  deleteAction,
  editAction,
  name,
  surname,
  email,
  image,
}) => {
  const [deleteMessage, setDeleteMessage] = useState(false);
  const { modalOpen, showModal, setModalOpen, closeModal } = useModal();
  const cancelBtnRef = useRef(null);

  const editAndClose = (e, id, name, surname, email, avatar) => {
    editAction(e, id, name, surname, email, avatar);
    closeModal();
  };

  const showDeleteMessage = (e) => {
    setDeleteMessage(true);
    showModal();
  };

  const showEditForm = (e) => {
    setDeleteMessage(false);
    showModal();
  };

  return (
    <div>
      <Button withIcon handleClick={showEditForm} buttonRef={cancelBtnRef}>
        <EditIcon className="action-icon" />
      </Button>
      <Button withIcon handleClick={showDeleteMessage} buttonRef={cancelBtnRef}>
        <BinIcon className="action-icon" />
      </Button>
      {modalOpen && (
        <Modal
          closeModal={() => closeModal(cancelBtnRef)}
          buttonRef={cancelBtnRef}
          setModalOpen={setModalOpen}
        >
          {" "}
          {deleteMessage ? (
            <span className="DeleteMessage">
              <p>Are you sure you want to delete this user?</p>
              <span className="DeleteMessage__buttons">
                <Button borderOnly handleClick={closeModal}>
                  cancel
                </Button>
                <Button
                  userId={userId}
                  handleClick={(e) => deleteAction(e, userId)}
                >
                  delete
                </Button>
              </span>
            </span>
          ) : (
            <Form
              formTitle="Edit user information"
              buttonLabel="Save changes"
              action={editAndClose}
              addNewUser={false}
              name={name}
              surname={surname}
              email={email}
              image={image}
              id={userId}
            ></Form>
          )}
        </Modal>
      )}
    </div>
  );
};

export default UserCardButtons;
