import React, { useRef, useEffect } from "react";
import { createPortal } from "react-dom";
import FocusLock from "react-focus-lock";
import PropTypes, { oneOfType } from "prop-types";

import Button from "../Button/Button";
import { ReactComponent as IconX } from "../../assets/icons/x.svg";
import { useOnClickOutside } from "../../utils/useOnClickOutside";

import "./modal.scss";

const Modal = ({children, setModalOpen, closeModal, buttonRef}) => {
    
  const modalRef = useRef(null);
  useOnClickOutside(modalRef, () => {
    setModalOpen(false);
    document.body.style.overflowY = "visible";
  });

  useEffect(() => {
    const onKeyDown = (e) => {
      if (e.key === "Escape") {
        closeModal();
      }
    };
    document.addEventListener("keydown", onKeyDown, false);
    return () => {
      document.removeEventListener("keydown", onKeyDown, false);
    };
  }, [closeModal]);

  const content = (
    <>
      <div className="overlay"></div>
      <FocusLock>
        <div className="modal" ref={modalRef} role="dialog" tabIndex="-1">
          <div className="modal__button-container">
            <Button
              iconX
              handleClick={() => {
                closeModal(buttonRef);
              }}
            >
              <IconX />
            </Button>
          </div>
          {children}
        </div>
      </FocusLock>
    </>
  );
  return createPortal(content, document.querySelector("#modal-root"));
};

Modal.propTypes = {
  children: oneOfType([PropTypes.element, PropTypes.array]),
  closeModal: PropTypes.func,
  setModalOpen: PropTypes.func,
  modalTitle: PropTypes.string,
};
export default Modal;
