import React, { useEffect, useState, useRef } from "react";
import axios from "axios";
import UserCard from "../../components/UserCard/UserCard";
import Button from "../../components/Button/Button";
import Modal from "../../components/Modal/Modal";
import Form from "../../components/Form/Form";

import { useModal } from "../../utils/useModal";
import { ReactComponent as ChevronLeft } from "../../assets/icons/chevron-left.svg";
import { ReactComponent as ChevronRight } from "../../assets/icons/chevron-right.svg";

import "./userList.scss";

const UserList = () => {
  const [userData, setUserData] = useState([]);
  const [numOfPages, setNumOfPages] = useState();
  const [currentPage, setCurrentPage] = useState(1);

  const { modalOpen, showModal, setModalOpen, closeModal } = useModal();
  const cancelBtnRef = useRef(null);
  const url = "https://reqres.in/api/users/";

  useEffect(() => {
    fetchUserData(currentPage);
  }, [currentPage]); // eslint-disable-line react-hooks/exhaustive-deps

  const fetchUserData = async (currentPage) => {
    try {
      const response = await fetch(`${url}?page=${currentPage}`);
      const data = await response.json();
      setUserData(data.data);
      setNumOfPages(data.total_pages);
      return data;
    } catch (error) {
      console.log("error", error);
    }
  };

  const handleDelete = (e, userId) => {
    axios.delete(`${url}${userId}`).then((res) => {
      let newList = userData.filter((user) => user.id !== userId);
      setUserData(newList);
    });
    closeModal();
  };

  const handlePost = (e, name, surname, email, avatar) => {

    let user = {
      id: userData.length + 1,
      first_name: `${name}`,
      last_name: ` ${surname}`,
      email: `${email}`,
      avatar: `${avatar}`,
    };

    axios.post(`${url}`, user);
    setUserData((userData) => [...userData, user]);
    closeModal();
  };

  const updateUser = (e, id, name, surname, email, avatar) => {
    const user = {
      id: id,
      first_name: `${name}`,
      last_name: ` ${surname}`,
      email: `${email}`,
      avatar: `${avatar}`,
    };
    axios.patch(`${url}/${id}`, user);
  
    let index = userData.findIndex((x) => x.id === user.id);
    let updatedList = [...userData]; 
    updatedList[index] = user;
    setUserData(updatedList);
  };

  return (
    <div className="UserList">
      <div className="UserList__action">
        <Button  handleClick={showModal} buttonRef={cancelBtnRef}>
          {" "}
          + new user
        </Button>
      </div>
      <div className="UserList__titles">
        <span>User</span>
        <span>Email</span>
        <span>Actions</span>
      </div>
      <div className="UserList__content">
        {userData.length &&
          userData.map((user) => {
            return (
              <UserCard
                key={user.id}
                name={user.first_name}
                surname={user.last_name}
                email={user.email}
                image={user.avatar}
                userId={user.id}
                deleteAction={handleDelete}
                editAction={updateUser}
              />
            );
          })}
      </div>
      <div className="UserList__pagination">
        <Button
          pagination
          disabled={currentPage === 1 ? true : false}
          handleClick={() => {
            setCurrentPage(currentPage - 1);
            fetchUserData(currentPage).then((data) => setUserData(data.data));
          }}
        >
          <ChevronLeft />{" "}
        </Button>
        <Button
          pagination
          disabled={currentPage === numOfPages ? true : false}
          handleClick={() => {
            setCurrentPage(currentPage + 1);
            fetchUserData(currentPage).then((data) => setUserData(data.data));
          }}
        >
          <ChevronRight />{" "}
        </Button>
      </div>
      {modalOpen && (
        <Modal
          closeModal={() => closeModal(cancelBtnRef)}
          buttonRef={cancelBtnRef}
          setModalOpen={setModalOpen}
        >
          {" "}
          <Form
            formTitle="Add a new user"
            buttonLabel="Add user"
            action={handlePost}
            addNewUser={true}
          ></Form>{" "}
        </Modal>
      )}
    </div>
  );
};

export default UserList;
