import React from "react";
import UserList from "../features/UserList/UserList";

import "./dashboard.scss";

const Dashboard = () => {
  return (
    <div className="dashboard">
      <div className="dashboard__header">
        <h1>User list </h1>
      </div>
      <div className="dashboard__content">
        <UserList />
      </div>
     
    </div>
  );
};

export default Dashboard;
